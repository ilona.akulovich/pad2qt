#ifndef LIST_H
#define LIST_H


#include "lobject.h"

template <class Anything>
class List
{
public:
    List();
    Anything test;
    Node<Anything>* first = nullptr;
    Node<Anything>* getLast();
    Node<Anything>* at(int n);
    Node<Anything>* operator[](int i);
    int size();
    void insert(int order, Anything body);
    void fitInPlace(Anything theBody);
    void push_back(Anything body);
    void pop();
};
#endif // LIST_H




