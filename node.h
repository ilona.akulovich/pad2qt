#ifndef NODE_H
#define NODE_H
#include "Booking.h"
#include <iostream>
using namespace std;

template <class Anything>
class Node
{
public:
    Node();
    Node(Anything body,Node* next,Node* prev);
    Anything body;
    Node<Anything>* next = nullptr;
    Node<Anything>* prev = nullptr;

};

#endif // NODE_H
