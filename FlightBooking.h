
#ifndef FLIGHTBOOKING_H
#define FLIGHTBOOKING_H
#include "general.h"
#include "Booking.h"
class FlightBooking: public Booking  {
public:
    FlightBooking(long, double, string, string, long, long, string, string, string, string, string w_oder_a);
    void print();
    virtual ~FlightBooking();
    virtual string showDetails();
    virtual vector<string> getDetails();
private:
//    long id;
//    double price;
//    string fromDate;
//    string toDate;
    string fromDest;
    string toDest;
    string airline;
    string window;//should be actually char but is there any difference?
};

#endif /* FLIGHTBOOKING_H */

