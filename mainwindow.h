#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTableWidgetItem>
#include <QListWidgetItem>
#include <QMessageBox>
#include <sstream>
#include "TravelAgency.h"
#include "general.h"
namespace Ui {
class MainWindow;
}
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    TravelAgency upandaway;
private slots:

    void on_actionRead_File_triggered();

    void on_actionShow_bookings_triggered();

    void on_actionClose_programm_triggered();

    void on_tableWidget_itemClicked(QTableWidgetItem *item);

    void on_tableWidget_cellClicked(int row, int column);


    void on_tableWidget_cellDoubleClicked(int row, int column);

    void on_actionTest1_triggered();

private:

    void setcal(int startday,int startmonth,int startyear,int endday,int endmonth,int endyear);
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
