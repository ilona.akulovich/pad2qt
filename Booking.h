
#ifndef BOOKING_H
#define BOOKING_H
#include "general.h"

class Booking  {
public:
    
    Booking ();
    Booking (long id, double price, string fromDate, string toDate, long tripID,long customerID,string customerName);
    char getType();
    long getTripID();
    long getID();
    double getPrice();
    string getCustomerName();    static long getCurrentID();
    virtual ~Booking ();
    vector<int> getStartDate();
    vector<int> getEndDate();
    string getBigString();
    virtual string showDetails()=0;
    virtual vector<string> getDetails()=0;
protected:
    char type;
    static long currentID;
    long id;
    double price;
    long tripID;
    long customerID;
    string customerName;
    string fromDate;
    string toDate;
    
};

#endif /* BOOKING_H */

