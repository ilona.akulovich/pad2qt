
#include "HotelBooking.h"

HotelBooking::HotelBooking(long id, double price,
                            string fromDate, string toDate,
                            long tripID, long customerID, string customerName,
                            string hotel, string town , string smoke)

:Booking (id,price,fromDate,toDate,tripID,customerID,customerName)//call BookingIG constructor for id
{
    this->hotel=hotel;
    this->town=town;
    this->smoke=smoke;
//cout<<smoke<<endl;
    this->type='H';
}
vector<string> HotelBooking::getDetails(){
    vector<string> details;
    details.push_back(hotel);
    details.push_back(town);
    details.push_back(smoke);
    return details;
}
HotelBooking::~HotelBooking() {
}
string HotelBooking::showDetails(){
    if(smoke=="1"){
        return "Raucherzimmer";
    }
    return "Nichtraucherzimmer";
}
void HotelBooking::print(){
    cout<<"HotelBooking "<<id<<endl;
}
