

#ifndef TRAVELAGENCY_H
#define TRAVELAGENCY_H

#include "FlightBooking.h"
#include "HotelBooking.h"
#include "CarReservation.h"
#include "general.h"
#include "Booking.h"
#include "Trip.h"
#include "Customer.h"
#include <QFileDialog>
#include "list.h"
    const int asciiF=70;
    const int asciiH=72;
    const int asciiR=82;
class TravelAgency {
public:
    TravelAgency();
    string readfile();
    Booking * findBooking(long id);
    Trip* findTrip (long id);
    Customer* findCustomer(long id);
    int createBooking(char, double, string, string, long, vector<string>);
    virtual ~TravelAgency();

    vector<string> getAllBookingStrings();
    vector<Booking *> getAllBookings();
    bool get_file_loaded();
private:
    List<Booking*> BookingsList;
    vector<Booking *> allBookings;
    vector<Customer*> allCustomers;
    vector<Trip*> allTrips;
    bool file_loaded = false;

};

#endif /* TRAVELAGENCY_H */

