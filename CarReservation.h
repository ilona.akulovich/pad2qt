

#ifndef CARRESERVATION_H
#define CARRESERVATION_H
#include "general.h"
#include "Booking.h"
class CarReservation : public Booking {
public:
    CarReservation(long, double, string, string, long, long, string, string, string, string, string insurance);
    void print();
    virtual ~CarReservation();
    string showDetails();
    vector<string> getDetails();
private:
//    long id;
//    double price;
//    string fromDate;
//    string toDate;
    string pickupLocation;
    string returnLocation;
    string company;
    string insurance;
};

#endif /* CARRESERVATION_H */

