#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionRead_File_triggered()
{
    cout<<"Read File"<< endl;
    string infomessage;

  infomessage =    upandaway.readfile();


QMessageBox::information(this, tr("Read File"),
                        tr(infomessage.c_str()));


}

void MainWindow::on_actionShow_bookings_triggered()
{
    int size=upandaway.getAllBookings().size();
    for (int i=0; i<size; i++){
    this->ui->tableWidget->insertRow( i );
    this->ui->tableWidget->setItem(i,2,new QTableWidgetItem(upandaway.getAllBookings().at(i)->getCustomerName().c_str()));
    this->ui->tableWidget->setItem(i,1,new QTableWidgetItem(  to_string (  upandaway.getAllBookings().at(i)->getPrice()  )  .c_str()));
    this->ui->tableWidget->setItem(i,0,new QTableWidgetItem(  to_string (  upandaway.getAllBookings().at(i)->getID()  )  .c_str()));
}
}

void MainWindow::on_actionClose_programm_triggered()
{
this->close();
}

void MainWindow::on_tableWidget_itemClicked(QTableWidgetItem *item)
{
}

void MainWindow::on_tableWidget_cellClicked(int row, int column)
{
}

void MainWindow::setcal(int startday,int startmonth,int startyear,int endday,int endmonth,int endyear){

    QDate date =  QDate(startyear,startmonth,startday);
    this->ui->calendarWidget_2->setSelectedDate(date);

     date =  QDate(endyear,endmonth,endday);
    this->ui->calendarWidget->setSelectedDate(date);
}

void MainWindow::on_tableWidget_cellDoubleClicked(int row, int column)
{
    cout << "Row " << row << " Column " << column << " ID " << this->ui->tableWidget->item(row,0)->text().toStdString() <<endl;
    long theID = this->ui->tableWidget->item(row,0)->text().toLong();

    this->ui->textBrowser_buchungsnummer->setText(this->ui->tableWidget->item(row,0)->text());

    int id = theID;//???
    Booking* currentBooking=this->upandaway.findBooking(id);
    this->setcal(currentBooking->getStartDate().at(2),
                 currentBooking->getStartDate().at(1),
                 currentBooking->getStartDate().at(0),
                 currentBooking->getEndDate().at(2),
                 currentBooking->getEndDate().at(1),
                 currentBooking->getEndDate().at(0) );
    this->ui->textBrowser_preis->setText(to_string(currentBooking->getPrice()).c_str());
    this->ui->textBrowser_kunde->setText(currentBooking->getCustomerName().c_str());
    this->ui->textBrowser_reisenummer->setText(to_string(currentBooking->getTripID()).c_str());
    char type=this->upandaway.findBooking(id)->getType();
    vector<std::string> currentDetails=currentBooking->getDetails();
    switch (type){
    case 'R':
    {
        this->ui->stackedWidget->setCurrentIndex(0);
        this->ui->textBrowser_firma->setText(QString::fromStdString(currentDetails.at(2)));
        this->ui->textBrowser_abholort->setText(QString::fromStdString(currentDetails.at(0)));
        this->ui->textBrowser_ruckgabeort->setText(QString::fromStdString(currentDetails.at(1)));
        this->ui->textBrowser_versicherung->setText(QString::fromStdString(currentDetails.at(3)));
        break;
    }
    case 'H':
    {
        this->ui->stackedWidget->setCurrentIndex(2);
        this->ui->textBrowser_hotel->setText(QString::fromStdString(currentDetails.at(0)));
        this->ui->textBrowser_stadt->setText(QString::fromStdString(currentDetails.at(1)));
        char istRaucher=currentDetails.at(2).at(0);
        switch (istRaucher) {
        case '1':
            this->ui->label_raucher->setText("Raucher");
            break;
        case '0':
            this->ui->label_raucher->setText("NichtRaucher");
            break;
        default:
            break;
        }
        break;
    }
    case 'F':
    {
        this->ui->stackedWidget->setCurrentIndex(1);
        this->ui->textBrowser_abflug->setText(QString::fromStdString(currentDetails.at(0)));
        this->ui->textBrowser_ziel->setText(QString::fromStdString(currentDetails.at(1)));
        this->ui->textBrowser_airline->setText(QString::fromStdString(currentDetails.at(2)));
        this->ui->textBrowser_sitzplatz->setText(QString::fromStdString(currentDetails.at(3)));
        break;
    }
    default:
    {
        cerr << "unknown error";
    }
}
}


void MainWindow::on_actionTest1_triggered()
{
    this->ui->groupBox_details1->hide();
}
