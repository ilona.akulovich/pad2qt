
#ifndef HOTELBOOKING_H
#define HOTELBOOKING_H
#include "general.h"
#include "Booking.h"
class HotelBooking:public Booking {
public:
    HotelBooking(long, double, string, string, long, long, string, string, string, string smoke);
    void print();
    virtual ~HotelBooking();
    string showDetails();
    vector<string> getDetails();
private:
//    long id;
//    double price;
//    string fromDate;
//    string toDate;
    string hotel;
    string town;
    string smoke;//should be bool but is it relevant?
};

#endif /* HOTELBOOKING_H */

